1) Добавить пакет к symfony приложению 
 composer require porandaikin/math-calculate-bundle
 2) Если нужно использовать другой парсер или считать другим калькулятором, добавить в service.yml
     porandaikin\MathCalculateBundle\Service\MathCalculate:
         autowire: true
         arguments:
              $parser: '@App\Service\ParserTest'
              $calculate: '@App\Service\CalculateTest'