<?php


namespace porandaikin\MathCalculateBundle\Controller;

use porandaikin\MathCalculateBundle\Exception\MathCalculateException;
use porandaikin\MathCalculateBundle\Service\MathCalculate;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MathCalculateController extends AbstractController
{
    /**
     * @Route("/calculator_solution",
     *  methods={POST}
     * )
     * @param Request $request
     * @param MathCalculate $calculate
     * @return JsonResponse
     */
    public function getCalculatorSolution(Request $request, MathCalculate $calculate): JsonResponse
    {
        $expression = $request->get('expression');
        $result = [
            'result' => '',
            'error' => '',
        ];
        $status = Response::HTTP_OK;
        try {
            $result['result'] = $calculate->getCalculatorSolution($expression);
        } catch (MathCalculateException $e) {
            $status = Response::HTTP_UNPROCESSABLE_ENTITY;
            $result['error'] = "Произошда ошибка : {$e->getMessage()}";
        }
        return $this->json($result, $status);
    }
}