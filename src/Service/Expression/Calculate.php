<?php

namespace porandaikin\MathCalculateBundle\Service\Expression;

use porandaikin\MathCalculateBundle\Exception\FunctionalException;
use porandaikin\MathCalculateBundle\Exception\InputExpressionException;
use porandaikin\MathCalculateBundle\Service\Expression\Operators\{
    AdditionExpression,
    DivisionExpression,
    MultiplicationExpression,
    OperatorExpression,
    SubtractionExpression
};
use porandaikin\MathCalculateBundle\Service\InterpreterContext;

class Calculate implements CalculateInterface
{
    /**
     * @param ExpressionDTO $expressionDTO
     * @return mixed
     * @throws InputExpressionException
     * @throws FunctionalException
     */
    public function process($expressionDTO)
    {
        $context = new InterpreterContext();
        $statement = $this->getGeneratedExpression($expressionDTO);
        $statement->interpret($context);

        return $context->lookUp($statement);
    }

    /**
     * @param ExpressionDTO $expressionDTO
     * @return OperatorExpression
     * @throws InputExpressionException
     * @throws FunctionalException
     */
    private function getGeneratedExpression(ExpressionDTO $expressionDTO): OperatorExpression
    {
        if (($expressionDTO->getFirstVariable() instanceof ExpressionDTO)
            || ($expressionDTO->getSecondVariable() instanceof ExpressionDTO)
        ) {
            throw new FunctionalException('Данный функционал не умеет расчитывать сложные выражения');
        }
        $classnameOperator = $this->getClassnameOperator($expressionDTO->getOperator());
        $statement = new $classnameOperator(
            new VariableExpression($expressionDTO->getFirstVariable()),
            new VariableExpression($expressionDTO->getSecondVariable())
        );

        return $statement;
    }

    /**
     * @param string $operator
     * @return string
     * @throws InputExpressionException
     */
    private function getClassnameOperator(string $operator): string
    {
        switch ($operator) {
            case '+':
                return AdditionExpression::class;
            case '-':
                return SubtractionExpression::class;
            case '*':
                return MultiplicationExpression::class;
            case '/':
                return DivisionExpression::class;
            default:
                throw new InputExpressionException('Данный оператор не расчитывается');
        }
    }
}