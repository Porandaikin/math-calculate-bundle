<?php

namespace porandaikin\MathCalculateBundle\Service\Expression;

use porandaikin\MathCalculateBundle\Exception\MathCalculateException;

interface CalculateInterface
{
    /**
     * @param $params
     * @return mixed
     * @throws MathCalculateException
     */
    public function process($params);
}
