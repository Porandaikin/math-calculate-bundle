<?php


namespace porandaikin\MathCalculateBundle\Service\Expression;


class ExpressionDTO
{
    /** @var ExpressionDTO|double */
    private $firstVariable;

    /** @var ExpressionDTO|double */
    private $secondVariable;

    /** @var string */
    private $operator;

    /**
     * @return ExpressionDTO|float
     */
    public function getFirstVariable()
    {
        return $this->firstVariable;
    }

    /**
     * @param ExpressionDTO|float $firstVariable
     * @return ExpressionDTO
     */
    public function setFirstVariable($firstVariable): self
    {
        $this->firstVariable = $firstVariable;

        return $this;
    }

    /**
     * @return ExpressionDTO|float
     */
    public function getSecondVariable()
    {
        return $this->secondVariable;
    }

    /**
     * @param ExpressionDTO|float $secondVariable
     * @return ExpressionDTO
     */
    public function setSecondVariable($secondVariable): self
    {
        $this->secondVariable = $secondVariable;

        return $this;
    }

    /**
     * @return ExpressionDTO|float
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @param string $operator
     * @return ExpressionDTO
     */
    public function setOperator(string $operator): self
    {
        $this->operator = $operator;

        return $this;
    }
}