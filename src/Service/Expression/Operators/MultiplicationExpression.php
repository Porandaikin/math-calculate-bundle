<?php


namespace porandaikin\MathCalculateBundle\Service\Expression\Operators;


use porandaikin\MathCalculateBundle\Service\InterpreterContext;

class MultiplicationExpression extends OperatorExpression
{
    protected function doInterpret(InterpreterContext $context, $leftOperand, $rightOperand)
    {
        $context->replace($this, $leftOperand * $rightOperand);
    }
}