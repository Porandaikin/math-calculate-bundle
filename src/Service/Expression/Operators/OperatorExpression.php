<?php


namespace porandaikin\MathCalculateBundle\Service\Expression\Operators;


use porandaikin\MathCalculateBundle\Service\Expression\Expression;
use porandaikin\MathCalculateBundle\Service\InterpreterContext;

abstract class OperatorExpression extends Expression
{
    protected $leftOperand;
    protected $rightOperand;

    public function __construct(Expression $leftOperand, Expression $rightOperand)
    {
        $this->leftOperand = $leftOperand;
        $this->rightOperand = $rightOperand;
    }

    public function interpret(InterpreterContext $context)
    {
        $this->leftOperand->interpret($context);
        $this->rightOperand->interpret($context);
        $resultLeft = $context->lookUp($this->leftOperand);
        $resultRight = $context->lookUp($this->rightOperand);
        $this->doInterpret($context, $resultLeft, $resultRight);
    }

    protected abstract function doInterpret(InterpreterContext $context, $resultLeft, $resultRight);
}