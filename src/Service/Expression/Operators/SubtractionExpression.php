<?php


namespace porandaikin\MathCalculateBundle\Service\Expression\Operators;


use porandaikin\MathCalculateBundle\Service\InterpreterContext;

class SubtractionExpression extends OperatorExpression
{

    protected function doInterpret(InterpreterContext $context, $leftOperand, $rightOperand)
    {
        $context->replace($this, $leftOperand - $rightOperand);
    }
}