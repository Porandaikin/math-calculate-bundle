<?php


namespace porandaikin\MathCalculateBundle\Service\Expression;


use porandaikin\MathCalculateBundle\Service\InterpreterContext;

class VariableExpression extends Expression
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function interpret(InterpreterContext $context)
    {
        $context->replace($this, $this->value);
    }
}
