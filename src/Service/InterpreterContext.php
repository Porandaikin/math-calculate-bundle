<?php

namespace porandaikin\MathCalculateBundle\Service;

use porandaikin\MathCalculateBundle\Service\Expression\Expression;

class InterpreterContext
{
    private $expressionStore = [];

    public function replace(Expression $exception, $value)
    {
        $this->expressionStore[$exception->getKey()] = $value;
    }

    public function lookUp(Expression $expression)
    {
        return $this->expressionStore[$expression->getKey()];
    }
}