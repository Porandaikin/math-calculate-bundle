<?php

namespace porandaikin\MathCalculateBundle\Service;

use porandaikin\MathCalculateBundle\Exception\MathCalculateException;
use porandaikin\MathCalculateBundle\Service\Expression\CalculateInterface;
use porandaikin\MathCalculateBundle\Service\Parser\ParserInterface;

class MathCalculate
{
    /** @var ParserInterface */
    private $parser;

    /** @var CalculateInterface */
    private $calculate;

    public function __construct(ParserInterface $parser, CalculateInterface $calculate)
    {
        $this->parser = $parser;
        $this->calculate = $calculate;
    }

    /**
     * @param $expression
     * @return mixed
     * @throws MathCalculateException
     */
    public function getCalculatorSolution($expression)
    {
        return $this->calculate->process($this->parser->parse($expression));
    }
}
