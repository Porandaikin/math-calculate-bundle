<?php

namespace porandaikin\MathCalculateBundle\Service\Parser;

use porandaikin\MathCalculateBundle\Exception\InputExpressionException;
use porandaikin\MathCalculateBundle\Service\Expression\ExpressionDTO;

class Parser implements ParserInterface
{
    /**
     * @param string $expression
     * @return ExpressionDTO
     * @throws InputExpressionException
     */
    public function parse(string $expression = ''): ExpressionDTO
    {
        if ($expression === '') {
            throw new InputExpressionException('Не введено исходное выражение');
        }
        $arrayParams = $this->getArrayParams($expression);
        if (count($arrayParams) > 3 || count($arrayParams) < 3) {
            throw new InputExpressionException('Выражение может принимать только два простых числа и оператор');
        }
        list($firstVariable, $operator, $secondVariable) = $this->getArrayParams($expression);

        return (new ExpressionDTO())->setFirstVariable($firstVariable)
            ->setSecondVariable($secondVariable)
            ->setOperator($operator);
    }

    /**
     * @param string $expression
     * @return array
     */
    private function getArrayParams(string $expression): array
    {
        $arrayAllParams = array_filter(str_split($expression), function ($param) {
            return trim($param) !== '';
        });
        return array_values($arrayAllParams);
    }
}
