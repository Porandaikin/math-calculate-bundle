<?php

namespace porandaikin\MathCalculateBundle\Service\Parser;

interface ParserInterface
{
    /**
     * @param string $expression
     * @return mixed
     */
    public function parse(string $expression = '');
}
