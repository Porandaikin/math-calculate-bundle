<?php

namespace porandaikin\MathCalculateBundle\Tests\Service\Expression;

use PHPUnit\Framework\TestCase;
use porandaikin\MathCalculateBundle\Exception\FunctionalException;
use porandaikin\MathCalculateBundle\Exception\InputExpressionException;
use porandaikin\MathCalculateBundle\Service\Expression\Calculate;
use porandaikin\MathCalculateBundle\Service\Expression\ExpressionDTO;

class CalculateTest extends TestCase
{
    /**
     * @throws \porandaikin\MathCalculateBundle\Exception\FunctionalException
     * @throws \porandaikin\MathCalculateBundle\Exception\InputExpressionException
     */
    public function testProcess()
    {
        $expressionDTO = (new ExpressionDTO())
            ->setFirstVariable('8')
            ->setSecondVariable('2')
            ->setOperator('/');
        $this->assertSame(4, (new Calculate())->process($expressionDTO));
    }

    /**
     * @throws FunctionalException
     * @throws \porandaikin\MathCalculateBundle\Exception\InputExpressionException
     */
    public function testFunctionalExceptionProcess()
    {
        $expressionDTO = (new ExpressionDTO())
            ->setFirstVariable(new ExpressionDTO())
            ->setSecondVariable(new ExpressionDTO())
            ->setOperator('/');

        $this->expectException(FunctionalException::class);
        $this->expectExceptionMessage('Данный функционал не умеет расчитывать сложные выражения');
        (new Calculate())->process($expressionDTO);
    }

    /**
     * @throws FunctionalException
     * @throws InputExpressionException
     */
    public function testExceptionOperatorProcess()
    {
        $expressionDTO = (new ExpressionDTO())
            ->setFirstVariable('8')
            ->setSecondVariable('2')
            ->setOperator('&');

        $this->expectException(InputExpressionException::class);
        $this->expectExceptionMessage('Данный оператор не расчитывается');
        (new Calculate())->process($expressionDTO);
    }
}
