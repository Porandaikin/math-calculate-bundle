<?php

namespace porandaikin\MathCalculateBundle\Tests\Service;

use PHPUnit\Framework\TestCase;
use porandaikin\MathCalculateBundle\Exception\MathCalculateException;
use porandaikin\MathCalculateBundle\Service\MathCalculate;
use porandaikin\MathCalculateBundle\Service\Parser\Parser;
use porandaikin\MathCalculateBundle\Service\Expression\Calculate;

class MathCalculateTest extends TestCase
{
    /**
     * @throws MathCalculateException
     */
    public function testGetCalculatorSolution()
    {
        $expression = '8/4';
        $mathCalculate = new MathCalculate(new Parser(), new Calculate());
        $this->assertSame(2, $mathCalculate->getCalculatorSolution($expression));
    }

    /**
     * @throws MathCalculateException
     */
    public function testFailedGetCalculatorSolution()
    {
        $this->expectException(MathCalculateException::class);
        $this->expectExceptionMessage('Не введено исходное выражение');
        $mathCalculate = new MathCalculate(new Parser(), new Calculate());
        $mathCalculate->getCalculatorSolution('');
    }
}
