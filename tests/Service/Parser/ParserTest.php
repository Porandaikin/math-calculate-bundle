<?php

namespace porandaikin\MathCalculateBundle\Tests\Service\Parser;

use PHPUnit\Framework\TestCase;
use porandaikin\MathCalculateBundle\Exception\InputExpressionException;
use porandaikin\MathCalculateBundle\Service\Expression\ExpressionDTO;
use porandaikin\MathCalculateBundle\Service\Parser\Parser;

class ParserTest extends TestCase
{
    /**
     * @throws InputExpressionException
     */
    public function testParse()
    {
        $expression = '8/2';
        $resultParse = (new Parser())->parse($expression);
        $expressionDTO = (new ExpressionDTO())
            ->setFirstVariable('8')
            ->setSecondVariable('2')
            ->setOperator('/');

        $this->assertEquals($expressionDTO, $resultParse);
    }

    /**
     * @throws InputExpressionException
     */
    public function testExceptionEmptyExpression()
    {
        $this->expectException(InputExpressionException::class);
        $this->expectExceptionMessage('Не введено исходное выражение');
        (new Parser())->parse('');
    }

    /**
     * @throws InputExpressionException
     */
    public function testExceptionCountParams()
    {
        $this->expectException(InputExpressionException::class);
        $this->expectExceptionMessage('Выражение может принимать только два простых числа и оператор');
        (new Parser())->parse('5*8/2');
    }
}
